### TO INSTALL DPC0 ON YOUR PC CHANGE THE FOLLOWING PATHS
##  USE ABSOLUTE PAHTS!!!
##  Remember to put the "/" (slash) character at the end of the FOLDERS PATHS [flagged with (!) FOLDER   ]


### DATA storage folder ###
# where the data you will analyise (input and outputs) will be stored. Check readme to see structure of the data storage folder
# (!) FOLDER
data_folder             = '/scratch/etrusso/mydata/'
## ^^^ Remember to put the "/" (slash) character at the end of the FOLDERS PATHS



####################################################
### ONLY IF YOU WANT TO USE BLAST #################
### blastp binary path (or type just 'blastp' if it is globally linked) ###
blast_binary            = '/u/e/erusso/ncbi-blast-2.2.30+/bin/blastp'
### blast database location ###
# this is the location of the blast database (blast db format) of your database; in our case, it was uniref50.
# you need to build it before the analysis (see readme - data preparation)
blast_database_folder     = '/scratch/etrusso/nur50db/db'
### blastp bumber of threads ###
# blastp can be run with parallel threads. We suggest to use 4 threads in case you have few cores (up to 8); otherwise a reasonable rule is to use NCORES-5 threads.
blast_num_threads       =  4

