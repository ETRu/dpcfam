#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <sys/time.h>
using namespace std;


double overlap(int s1,int e1, int s2, int e2){
   double d;
   if(e2>=e1 && e1>=s1 && s1>=s2) d=(e1-s1)*1./(e2-s2);
   if(e1>=e2 && e2>=s2 && s2>=s1) d=(e2-s2)*1./(e1-s1);
   if(e2>=e1 && e1>=s2 && s2>=s1) d=(e1-s2)*1./(e2-s1);
   if(e1>=e2 && e2>=s1 && s1>=s2) d=(e2-s1)*1./(e1-s2);
   if(s2>e1)d=0.;
   if(s1>e2)d=0.;
   return d;
}


double asymm(int s1,int e1, int s2, int e2){
   double d;
   int l1,  lmax,lmin;
   int smax, emin, intersection;
   if(s2>=e1) return 0.;
   if(s1>=e2) return 0.;
     
   l1= e1-s1;
   
   if(s1>s2)smax=s1;
   else smax=s2;
   if(e1<e2) emin=e1;
   else emin=e2;
   
   intersection=emin-smax;
   
   d=float(intersection)/l1;
   
   return d;
}


int main(int argc, char *argv[]){
   //char filetr[1];
   //cin >> filetr; 
  
   int totals=0; 
   int totclusters=0;
   int matdim=0;
  
   
   string fileinput = argv[1];
   
   ifstream filein;
	  filein.open(fileinput.c_str(),ios::in);
   
   string line;
   vector <int> id_q,id_s,st_s,en_s,st_q,en_q, icl, cl_pop;
   vector <double> ev,r_sc;
   int nl=0,clo=0,cl,nclust=0;
   while (getline(filein,line)) {
      istringstream streamA(line);
      int it01, it02, it1,it2,it3,it4,it5,it6,cl;
      double cd1,cd2;
      streamA >> it01 >> it02 >> it1>> it2>> it3>> it4>> it5>> it6>>cd1>>cd2>>cl;
      icl.push_back(it01);     
      cl_pop.push_back(it02);     
      id_q.push_back(it1);     
      id_s.push_back(it2);     
      st_q.push_back(it3);     
      en_q.push_back(it4);     
      st_s.push_back(it5);     
      en_s.push_back(it6);     
      r_sc.push_back(cd1);     
      ev.push_back(cd2);    
      ++totals; 
   }
   filein.close();
   
   for(int i=0; i<totals; ++i) {if(icl[i]>totclusters) totclusters=icl[i];}
   
   //cout << totclusters << endl;
   
   matdim=totclusters*totclusters;
   
   vector <double> dmat(matdim,0.);
   vector <double> amat(matdim,0.);
   vector <int> normd(matdim,0);
   vector <int> norma(matdim,0);
   vector <int> thiscl_pop(totclusters,1);
   
   int working_sseq=id_s[0];
   int from,to;
   from=0;
   for(int al=0; al<totals; ++al){  
   
    if(working_sseq!=id_s[al]){
     to=al;  
     for(int i=from; i<to; ++i){//cout << from << " " << to << " " << i << endl;
      for(int j=from; j<to; ++j){
     
       int clID_i, clID_j;
       clID_i=icl[i]-1;       
       clID_j=icl[j]-1;
       thiscl_pop[clID_i]=cl_pop[i];
       thiscl_pop[clID_j]=cl_pop[j];
       
       // calcolo distanza
        double ov=overlap(st_s[i],en_s[i],st_s[j],en_s[j]);
        if(ov>=0.8) { 
        dmat[clID_i*totclusters+clID_j]+=1;  // XXX
        normd[clID_i*totclusters+clID_j]+=1; 
        }
        //calcolo distanza asimmetrica
        amat[clID_i*totclusters+clID_j]+=asymm(st_s[i],en_s[i],st_s[j],en_s[j]); norma[clID_i*totclusters+clID_j]+=1;

       }
     }
     from=to;
     working_sseq=id_s[al];
    } 
   }
   // FINAL STEP
   to=totals;  
     for(int i=from; i<to; ++i){//cout << from << " " << to << " " << i << endl;
      for(int j=from; j<to; ++j){
     
       int clID_i, clID_j;
       clID_i=icl[i]-1;       
       clID_j=icl[j]-1;
       
       if(clID_i!=clID_j){
        thiscl_pop[clID_i]=cl_pop[i];
        thiscl_pop[clID_j]=cl_pop[j];
        
        // calcolo distanza
        double ov=overlap(st_s[i],en_s[i],st_s[j],en_s[j]);
        if(ov>=0.8) { 
        dmat[clID_i*totclusters+clID_j]+=1;  //XXX
        normd[clID_i*totclusters+clID_j]+=1; 
        }
        //calcolo distanza asimmetrica
        amat[clID_i*totclusters+clID_j]+=asymm(st_s[i],en_s[i],st_s[j],en_s[j]); norma[clID_i*totclusters+clID_j]+=1;
        }
        
       }
     }
   
   
   
   
   
  
  for(int i=0; i<totclusters; ++i){
   for(int j=0; j<totclusters; ++j){
    int minpopij;
    minpopij=thiscl_pop[i]; if(thiscl_pop[j]<minpopij) minpopij=thiscl_pop[j];

    int maxpopij;
    maxpopij=thiscl_pop[i]; if(thiscl_pop[j]>maxpopij) maxpopij=thiscl_pop[j];

    dmat[i*totclusters+j]= 1.- dmat[i*totclusters+j]/minpopij;
    amat[i*totclusters+j]= 1.- amat[i*totclusters+j]/minpopij;
    
    // XXX correzione perche` ci sono stati dei problemi con i clustering primari che in questo momento non posso correggere.
    if(dmat[i*totclusters+j]<0.) dmat[i*totclusters+j]=0;
    if(amat[i*totclusters+j]<0.) amat[i*totclusters+j]=0;
 
   }
  }  
  
    
    
    //for(int i=0;i<nclust;++i) {cout<< i <<" " << icl_orig[i] <<" " << idq_orig[i] << endl;  }
   for(int i=0; i<totclusters; ++i){
    for(int j=i; j<totclusters; ++j){
     cout << i << " " << j << " " << dmat[i*totclusters+j] << " " << amat[i*totclusters+j] << " " << normd[i*totclusters+j] << " " << norma[i*totclusters+j] << " " << thiscl_pop[i] << " " << thiscl_pop[j] << endl;
     //cout << j << " " << i << " " << dmat[j*totclusters+i] << " " << amat[j*totclusters+i] << " " << normd[j*totclusters+i] << " " << norma[j*totclusters+i] << " " << thiscl_pop[j] << " " << thiscl_pop[i] << endl;
    }
   }
   
   
   return 0;
}

