#! /usr/bin/env python3
import sys
import os
import argparse
import libs_DPCF
import install_DPCF

# This is just to COLOR TEXT
###################################
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m\033[40m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
###################################

# Get the "install folder"
install_folder=os.path.dirname(os.path.abspath(__file__))+"/"


####  ARGUMENT PARSER
parser = argparse.ArgumentParser()
parser.add_argument('-inst', '--install', help='Installation: builds main data_folder folders for the dataset to anlyze.', action="store_true")
parser.add_argument('-i', '--input_dataset', help=' the dataset to analyse ')
parser.add_argument("-b", "--blast", help='run blast on raw fasta data. Input folder: "fasta-in", output folder: "blasted-outputs"', action="store_true")
parser.add_argument("-cl1", "--primary_clustering", help='run primary clustering, detects portions in the proteins that align often. Input folder: "blasted-outputs", output folder: "output1", clustering logs stored in "logs" folder', action="store_true")
parser.add_argument("-cl2", "--secondary_clustering", help='run secondary clustering, metaclustering between primary clusters. Input folder: "output1", output folder: "output2"', action="store_true")
parser.add_argument("-bMC", "--build_MCs", help='Build final metaclusters fastas. Input folder: "output2", "output1", "blasted-outputs; output folder "MCs', action="store_true")
parser.add_argument("-all", "--run_all", help='run the whole analysis using raw data', action="store_true")
parser.add_argument("-allnob", "--run_all_but_blast", help='run the whole analysis using aligned data.', action="store_true")



args = parser.parse_args()
sequence_set=args.input_dataset

# Set args according to special args: -all -allnob and -test (-i is not required if testing!)
if args.run_all:
    args.blast=True
    args.primary_clustering=True
    args.secondary_clustering=True
    args.build_MCs=True

if args.run_all_but_blast:
    args.primary_clustering=True
    args.secondary_clustering=True
    args.build_MCs=True

if not args.input_dataset:
    if not args.use_test:
        print( bcolors.FAIL + '\n ERROR: No dataset to analyze\n   Please use -i dataset  *or*  -test \n' + bcolors.ENDC)
        sys.exit()


## Define data folder

print('\n--- Checking if data folder already exists')
if not os.path.isdir(install_DPCF.data_folder):
    print('Folder {} Not exists ===== > I create it.'.format(install_DPCF.data_folder)) 
    os.makedirs(install_DPCF.data_folder)
else:
    print("OK")

## Define dataset folder
print('\n--- Checking if dataset folder already exists')
dataset_folder=install_DPCF.data_folder + sequence_set + '/'
if not os.path.isdir(dataset_folder):
    print('Folder {} Not exists ===== > I create it.'.format(dataset_folder)) 
    os.makedirs(dataset_folder)
else:
    print("OK")

# Define WORKING folders (upper_folder/dataset )
working_folders = dict()
working_folders['fastas-in'] = dataset_folder + 'fastas-in/'
working_folders['blasted_outputs'] = dataset_folder + 'blasted-outputs/'
working_folders['output1'] = dataset_folder + 'output1/'
working_folders['output2'] = dataset_folder + 'output2/'
working_folders['logs'] = dataset_folder + 'logs/'
working_folders['MCs'] = dataset_folder + 'MCs/'

# Check if working folders exists; if not, create them (unless some exception that stops the program)
print('\n--- Checking inner folders')
for working_folder, working_folder_with_path  in working_folders.items():
    if not os.path.isdir(working_folder_with_path):
        print('Folder {}/{} Not exists ===== > I create it.'.format(working_folder_with_path.split('/')[-3],working_folder_with_path.split('/')[-2] )) 
        os.makedirs(working_folder_with_path)
    else:
        print('Folder {} exists, OK. '.format(working_folder)) 
  
 

#############################################################################
####  ANALYSIS STARTS HERE
#############################################################################
 

### (0) RUN BLAST 
print('\n--- STEP 0: BLAST')
if args.blast:
    try:
        libs_DPCF.blast_proteins_fasta_vs_database_MONOFILE(working_folders['fastas-in'],working_folders['blasted_outputs'], install_DPCF.blast_database_folder, install_DPCF.blast_binary, install_DPCF.blast_num_threads )
    except Exception as e:
        print(bcolors.FAIL + 'Error running BLAST: ' + bcolors.ENDC  +'{}'.format(e))
        sys.exit()
    print('\nBlasting Done.\n')
else:
    print('Not using blast; if you want to blast use "-b" or "-all"')
 
 

### (1) PRIMARY CLUSTERING
print('\n--- STEP 1: PRIMARY CLUSTERING')
if args.primary_clustering:
    try:
        libs_DPCF.run_primary_clustering_MONOFILE(install_folder, working_folders['blasted_outputs'],working_folders['output1'],working_folders['logs'])
    except Exception as e:
        print(bcolors.FAIL+'Error running primary clustering:' + bcolors.ENDC  +' {}'.format(e))
        raise
    # GENERATE CLSINGLE FILES
    print('Generating .clsingle files...')
    try:
        libs_DPCF.generate_clsingle_files(working_folders['output1'])
    except Exception as e:
        print(bcolors.FAIL + 'Error Generating .clsingle files: ' + bcolors.ENDC  +'{}'.format(e))
        raise
    print('\nPrimary Clustering Done.\n')
else:
    print('Not doing primary clustering; if you want to do primary clustering use "-cl1" or "-all" or "-allnob"')



### (2.a) GENERATE ADJ MATRIX for II CLUSTERING (uses NEWasym_link.cpp)
print('\n--- STEP 2.a:  GENERATE DISTANCE  MATRIX for Secondary ')
if args.secondary_clustering:
    try:
        libs_DPCF.generate_dmat(install_folder,working_folders['output1'],working_folders['output2'],working_folders['logs'])
    except Exception as e:
        print(bcolors.FAIL + 'Error generating adj matrix using NEW_asymlink.cpp:' + bcolors.ENDC  +' {}'.format(e))
        raise
else:
    print('Not doing secondary clustering; if you want to do secndary clustering use "-cl2" or "-all" or "-allnob"')    



### (2.b) Run CLUSTERING using dmat (obtained previously)
print('\n--- STEP 2.b:  Run SECONDARY CLUSTERING')
if args.secondary_clustering:
    try:
        DPA_mycluster_family_xylist=[]           # XXX
        DPA_mycluster_family_xylist=libs_DPCF.run_cpp_IIclustering(install_folder,working_folders['output1'],working_folders['output2'],working_folders['logs'])
    except Exception as e:
        print( )
        print( bcolors.FAIL + 'ERROR running DPA clustering:' + bcolors.ENDC  +'{}'.format(e))
        raise
else:
    print('Not doing secondary clustering; if you want to do secndary clustering use "-cl2" or "-all" or "-allnob"')  

### (3.a) Merge Metaclusters
print('\n--- STEP 2.c: MERGE METACLUSTERS')
if args.secondary_clustering:
    libs_DPCF.merge_MCs(working_folders['output2'])
else:
    print('Not doing secondary clustering; if you want to do secndary clustering use "-cl2" or "-all" or "-allnob"')  

#sys.exit()

print('\n--- STEP 4: GENERATE FINAL MC FILES')
if args.build_MCs:
    libs_DPCF.write_MCs_fastas(working_folders['output1'],working_folders['output2'], working_folders['MCs'])
else:
    print('Not building MC fasta files; if you want to do build MCs fastas "-bMC or "-all" or "-allnob"')  

sys.exit()
 
