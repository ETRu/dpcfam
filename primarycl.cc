#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <vector> 
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;



//---------------------------------------------------------------------------------------------------------
// Aligned segment struct
//---------------------------------------------------------------------------------------------------------
struct segment {
		
  int qstart;
  int qend;
  int sstart;
  int send;
  int len;
  int score;
  int intsid;
  int intqid;
  string sseq;
  string qseq;
 
  double rho;
  double delta;
  double evalue;
  
  // boolean "is a center" (0: no, 1: yes)
  int isc;


} ;






//---------------------------------------------------------------------------------------------------------
// SEGMENTS DISTANCE on the QUERY
//--------------------------------------------------------------------------------------------------------- 
double dist(segment i, segment j){
	int hi, lo;
	double inte, uni;
	int istart, iend, jstart, jend;
    istart= i.qstart; iend= i.qend; jstart=j.qstart; jend=j.qend;
    //calculate intersection
    inte=0;
	hi=iend;
    if(jend<hi) hi=jend;
    lo=istart;
    if(jstart>lo) lo=jstart;
    if(hi>lo) inte=hi-lo;
    //calculate union
    hi=iend;
    if(jend>hi) hi=jend;
    lo=istart;
    if(jstart<lo) lo=jstart;
    uni=hi-lo;
return (uni-inte)/uni;
}










//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
/////////////////////////////////   M   A   I   N    //////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------


// USAGE:  ./a.out  input  output(rhosanddeltas) output(clusters) dpar gpar
// input foramat:
// qseqid sseqid qstart qend sstart send qlen slen length pident evalue bitscore score gapopen gaps qseq sseq

int main(int argc, char** argv) {

    srand (200419901);

	double dpar; // kernel size parameter
	double gpar; // gamma gap parameter
	// READ FROM OUTSIDE
	dpar=atof(argv[4]);
	gpar=atof(argv[5]);
	
	int ndata;
	vector<segment> mysegments;
	vector<int> centres;
	
	ifstream infile; // input 
	ofstream outfile; // output


	
	// apri input
	infile.open (argv[1]);
	if (!infile.is_open()) { cout << "INFILE NOT GOOD" << endl; return 1; }
	
	cout << "\n \n    Input File:        " << argv[2] << endl;
 
 	
 	// ++++++++++++++++++++++++++++++++++ READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
    string line; 
	while( getline(infile, line)) {
	    
	        int num_qseqid, num_sseqid;  
		int  qstart, qend, sstart, send, length;
		double evalue;
		int score;
		string qseq;
		string sseq;
		
		//input that i wo't use ( default STRING so no parsing problem)
    	         string  qlen, slen, pident, bitscore, gapopen, gaps ;
    	
		istringstream iss(line); 
		iss >> num_qseqid >> num_sseqid >> qstart >> qend >> sstart >> send >> qlen >> slen >> length >> pident >> evalue >> bitscore >> score >> gapopen >> gaps >> qseq >> sseq;
		
		
		segment temp;
		
		temp.qstart=qstart;
		temp.qend=qend;
		temp.sstart=sstart;
		temp.send=send;
		temp.len=length;
		temp.score=score;
		temp.evalue=evalue;
		
		temp.sseq=sseq;
		temp.qseq=qseq;
		
		
		temp.intsid=num_sseqid;
  		temp.intqid=num_qseqid;
  		
 		
		
  		// default value 0
		temp.rho=0;
		temp.delta=0;
		temp.isc=0;
		
			
		mysegments.push_back(temp);
		
	}
	
	infile.close();
    
    
 	// ++++++++++++++++++++++++++++++++++ END READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
    
    
    
    
	cout << "- segments read : " << mysegments.size() << endl;
   
   
   
   
   
   
  
   
	// -----------------------------------------------------------------------------------------------------------------------   
    // CLEAN ALIGNMENTS ON THE SAME SEARCH THAT ARE TOO CLOSE (keep the best one wrt score)
   
   	vector<int> todel;    
    for(int i=0; i<mysegments.size();++i) {
	    for(int j=i+1; j<mysegments.size();++j) {
	    	if(mysegments[i].intsid == mysegments[j].intsid){
	    			if( dist( mysegments[i], mysegments[j]) < dpar ) {
	    			  //check if it is alterdy there
	    			  int repetita=0;
	    			  int indextodel;
	    			  
	    			  if(mysegments[i].score > mysegments[j].score) { indextodel=j; }
	    			  else { indextodel=i; }
	    			  
	    			  for(int k=0; k<todel.size(); ++k) {if(todel[k] == indextodel) repetita=1;}
	    			  
	    			  if(repetita==0){
	    					todel.push_back(indextodel);
	    			  }
	    			      			
	    			}
	    	}
	    
	    }

    }

     
    cout << "   TOO MUCH SELF-OVERLAP ON :" << todel.size() << " ELEMENTS\n   REMOVING: ";
    for(int td=0; td<todel.size(); ++td) { cout << todel[td] << " "; }
    cout << endl;

	for(int td=todel.size()-1; td>=0; --td ){  mysegments.erase (mysegments.begin()+td);  }
   

	cout << "- actual segments : " << mysegments.size() << endl;   
   
    if (mysegments.size()<20) return 1; //not enough segments to cluster, exit with error
   
    outfile.open (argv[2]);
	if (!outfile.is_open()) { cout << "OUTFILE NOT GOOD" << endl; return 1; }
   


    ndata =  mysegments.size();
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // GENERO RHO
    
    // self-density initialization, wighted wrt score/(qend-qstart)
    for(int i=0;i<mysegments.size();++i) 	mysegments[i].rho = (float(mysegments[i].score))/ (mysegments[i].qend - mysegments[i].qstart)   ;
    
        for(int i=0; i<mysegments.size();++i) { 
     		for(int j=i+1; j<mysegments.size();++j){
    		double d, g;
   			
   			// compute distance
   			d = dist( mysegments[i], mysegments[j]) + 0.00001*((double) rand() / (RAND_MAX));
   			
   			// linear kernel density estimation
   			if(d>=dpar) g=0; else g=1.;

    		
    		mysegments[i].rho += g;
    		mysegments[j].rho += g;
    		
    	}
    }
    

   
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE DELTA
        
    for(int i=0; i<mysegments.size();++i) {
    	//double delta=1000;
    	mysegments[i].delta=1000;
    	for(int j=0; j<mysegments.size();++j){
    		
    		if(i==j) continue;
    		if(mysegments[j].rho <= mysegments[i].rho) continue;
    		
    		double d;
    		d = dist( mysegments[i], mysegments[j]) + 0.00001*((double) rand() / (RAND_MAX));
    		
    		if( d < mysegments[i].delta ) mysegments[i].delta=d; 
    		
    	}
    	
    	
        	outfile << "0 " << mysegments[i].intqid << " " << "notxt" << " " << mysegments[i].intsid << " " << "notxt" << " " << mysegments[i].rho  << " " << mysegments[i].delta  << " " << mysegments[i].qstart  << " " << mysegments[i].qend << " " << mysegments[i].sstart  << " " << mysegments[i].send << endl;
        	
    }
    
   
   
   
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // FIND FIRST 20 CENTERS CANDIDATES
    
   for(int npc=0; npc<20; ++npc){
	
		double maxgamma=0;
		int maxi=-1;
		for(int i=0; i<mysegments.size();++i) {
		 double gamma; 
		 gamma = mysegments[i].rho * mysegments[i].delta;
		 if( (gamma > maxgamma) && mysegments[i].isc !=1 ) {maxgamma = gamma; maxi=i;}
		}
		
		mysegments[maxi].isc=1;
		centres.push_back(maxi);
						
    }





    // -----------------------------------------------------------------------------------------------------------------------    
    // SELECT TRUE CENTERS USING THE GAP PARAMETER
    
    int lastgap=0;
    for(int c=0; c<centres.size()-1; ++c){
		double gamma1 = mysegments[ centres[c] ].rho * mysegments[ centres[c] ].delta;
		double gamma2 = mysegments[ centres[c+1] ].rho * mysegments[ centres[c+1] ].delta;
		
		if( -log(gamma2/gamma1) > gpar ) lastgap=c;
		
		cout << c << " " << centres[c] << " " << lastgap << " gamma1 " << gamma1 << " gamma2 " << gamma2 << " LOGgamma1 " << log(gamma1) << " LOGgamma2 " << log(gamma2) << "gamma1 " << gamma1 << " log(gamma2/gamma1) " << log(gamma2/gamma1) << endl;
	}    
	 
	
    // DELETE ALL CENTERS AFTEER THE LAST SIGNIFICANT GAP
    for(int c=lastgap+1; c<centres.size(); ++c){
  	  cout<< "cleaning " << centres[c] << endl; mysegments[centres[c]].isc=0;
    }
    centres.erase(centres.begin()+lastgap+1, centres.begin()+centres.size());
  	
  	
	cout << " lastgap " << lastgap << endl;
	for(int c=0; c<centres.size(); ++c) cout << centres[c] << " " ;
  	
  	
  	//WRITE
  	for(int c=0; c<centres.size(); ++c)
  	outfile << "1 " << mysegments[centres[c]].intqid << " " << "notxt" << " " << mysegments[centres[c]].intsid << " " << "notxt"  << " " << mysegments[centres[c]].rho  << " " << mysegments[centres[c]].delta  << " " << mysegments[centres[c]].qstart  << " " << mysegments[centres[c]].qend << " " << mysegments[centres[c]].sstart  << " " << mysegments[centres[c]].send << endl;
   
   
   
   
   
      outfile.close();
   

   
   
   

    // -----------------------------------------------------------------------------------------------------------------------    
    //  CLUSTERING
    
    outfile.open (argv[3]);
	if (!outfile.is_open()) { cout << "OUTFILE NOT GOOD" << endl; return 1; }
    
    int incl=0;
    for(int i=0; i<mysegments.size(); ++i){
    	double mind=1000001;
    	int cl=-1;
    	for(int c=0; c<centres.size(); ++c){
    		double d = dist( mysegments[i], mysegments[centres[c]]) + 0.00001*((double) rand() / (RAND_MAX));
    		//outfile << i << " " << centres[c] << " " << d << endl;
    		if(d < mind) { mind=d; cl=c; }
    	}
    
    	if(cl>-1){
    	double d = dist( mysegments[i] , mysegments[ centres[cl] ] ) ;
    	  
    	    	  
    	  if( d < dpar ){
    	  outfile << 
    	  
    	  i << " " <<  
    	  
    	  centres[cl] << " " << 
    	  cl << " "  << 
    	  
    	  d <<  " | " <<      
    	  
    	  mysegments[i].intsid << " " << 
    	  "notxt" << " " <<
	      mysegments[i].sstart << " " << 
    	  mysegments[i].send << " | " <<
    	   
    	   
    	  mysegments[i].intqid << " " << 
    	  "notxt" << " " << 
    	  mysegments[i].qstart << " " << 
    	  mysegments[i].qend << " | " <<  	
    	    
    	   mysegments[i].isc << " | " <<
    	   
    	   mysegments[i].sseq << " " << 
    	   mysegments[i].qseq << " " <<
    	    
    	    (float(mysegments[i].score))/ (mysegments[i].qend - mysegments[i].qstart)   << " " <<
    	    
    	     mysegments[i].evalue << " " <<
    	   endl;
    	   
    	  ++incl;
    	  }
    	}
    	else {
    	cout << " \n\nERROR CUSTERING" << endl;
    	return 2;    	
    	}
    
    }
   
   outfile.close();

   cout << "\n\n INCL=" << incl << " NDATA=" << ndata << " diff:" << ndata-incl << " ratio:" << double(incl)/ndata << endl; 



	cout << endl;   
	return 0;




}



