#! /usr/bin/env python3
import sys
import os
from utils import os_shell
import time
import numpy as np


### Text Coloring
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


######################################################################
### UTILITIES 
######################################################################

### File listing
def list_files(directory,extension=None):
    r = []
    for root, ds, files in os.walk(directory):
        for name in files:
            if extension:
                if not name.endswith(extension):
                    continue
                r.append(os.path.join(root, name))
    return r
 
def list_sorted_files(directory,extension=None):
    r = []
    for root, ds, files in os.walk(directory):
        for name in sorted(files):
            if extension:
                if not name.endswith(extension):
                    continue
                r.append(os.path.join(root, name))
    return r


### Find the subset of unique appareances in a list (order preserving) ( https://www.peterbe.com/plog/fastest-way-to-uniquify-a-list-in-python-3.6 )
def my_unique(seq, idfun=None): 
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result



### ERROR HANDLING when using OS COMMANDS from utils
def error_handler_os_command(std_out,std_err,std_ext,command):
    if std_ext!=0:
        print( bcolors.FAIL + 'Error running {}'.format(command) + bcolors.ENDC )
        raise ValueError('{} \n EXITED ERROR {}'.format(repr(std_err), repr(std_ext)))


### NMI - from a list of 2 values, x and y, compute MUTUAL INFORMATION, NORMALIZED MUTUAL INFORMATION
def MutualInformation(data):

    xdata=[d[0] for d in data]
    ydata=[d[1] for d in data]
 
    x_classes=my_unique(xdata)
    y_classes=my_unique(ydata)
    xy_classes=[(x,y) for x in x_classes for y in y_classes]
 
    p_x=[]
    p_y=[]
    p_xy=[]
 
    for c in x_classes:
        p_x.append( len([x for x in xdata if x==c])/len(xdata) )
    for c in y_classes:
        p_y.append( len([y for y in ydata if y==c])/len(ydata) ) 
    for c in xy_classes:
        elements=[d for d in data if d[0]==c[0] and d[1]==c[1]]
        p_xy.append( len(elements) / len(data) )


    MI=0
    for i,xc in enumerate(x_classes):
        for j,yc in enumerate(y_classes):
            pxy= float(len([d for d in data if d[0]==xc and d[1]==yc])) / len(data)
            if pxy!=0:
                MI = MI +  pxy * np.log (  pxy / (p_x[i]*p_y[j]) )

    Sx=0
    for i,xc in enumerate(p_x):
        if p_x[i]!=0:
            Sx = Sx + p_x[i] * np.log(p_x[i])
   
    Sy=0
    for i,yc in enumerate(p_y):
        if p_y[i]!=0:
            Sy = Sy + p_y[i] * np.log(p_y[i])

    Savg= 0.5*(Sx+Sy)
    Savg=-Savg
 
    if Savg!=0:
        NMI=MI/Savg
    else:
        NMI=0
 
    return MI, -Sx, -Sy, NMI
 
 
### Compute overlap of two segments, i (i_start,iend) and j (j_start,j_end)
# WARNING! INTEGER or FLOAT ARGUMENTS!
def calculate_segments_overlap(istart,iend,jstart,jend):
 
 hi=iend
 if jend<hi :
  hi=jend
   
 lo=istart
 if jstart>lo :
  lo=jstart
 
 inte=0
 if hi>lo :
  inte=hi-lo
 
 hi=iend
 if jend>hi:
  hi=jend

 lo=istart 
 if jstart<lo :
  lo=jstart
   
 uni=hi-lo
 
 result=float(inte)/uni
 
 return result







######################################################################
### CLUSTERING PIPELINE MODULES
######################################################################

### Run blast using all.fasta (fastas-in folder) as infile. 
def blast_proteins_fasta_vs_database_MONOFILE(fastas_in_folder_with_path, blast_output_folder_with_path, blast_database_location_with_path, blast_binary, blast_num_threads):
 
 # blast formatting command. this defines the outputs that blast will give 
 output_format_arg = '-outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue bitscore score gapopen gaps qseq sseq"' 
 
 # set outfile
 output_file_with_path = blast_output_folder_with_path +'all_blasted_out.txt'
 
 # chech if we have the all.fasta file 
 fasta_files_wp  = list_sorted_files(fastas_in_folder_with_path,".fasta")
 already_have_alldotfasta=False
 for f in fasta_files_wp:
  if f.split('/')[-1]=='all.fasta':
   already_have_alldotfasta=True
 # if all.fasta not found, raise error
 if not already_have_alldotfasta:
  raise ValueError('ERROR BLASTING: no "all.fasta" file found - exiting')
 else:
  print('all.fasta found')

 #BLAST command:
 command = '{} -query {}all.fasta -db {} -evalue 0.1 -max_target_seqs 5000000 -out {} {} -num_threads {}'.format(blast_binary,
                                                                                                                  fastas_in_folder_with_path,
                                                                                                                      blast_database_location_with_path, 
                                                                                                                    output_file_with_path,
                                                                                                                      output_format_arg, 
                                                                                                                      blast_num_threads)                                                            
 # RUN BLAST
 print('Running command "{}"'.format(command))
 blast_out, blast_err, blast_exit =os_shell(command,capture=True)
 error_handler_os_command(blast_out,blast_err,blast_exit,command)
 

### run primary clustering
def run_primary_clustering_MONOFILE(install_folder,blasted_input_folder_with_path, output_folder, logs_folder):

 distance_parameter = 0.2
 gap_parameter = 0.5
 
 print(bcolors.OKGREEN +"Running primary clustering using the following parameters: \n distance_parameter = {} \n gap_parameter={}".format(distance_parameter,gap_parameter)+bcolors.ENDC)
 
 output_exe_with_path = install_folder + 'step1.x'

 blasted_input_MONOfile_wp  = blasted_input_folder_with_path +'all_blasted_out.txt'
 
 # clean the output folder from clsingle files
 old_files_list=list_sorted_files(output_folder,"clsingle")
 for f in old_files_list:
  os.remove(f)
 
 # clean the input folder from .txts files
 old_files_list=list_sorted_files(blasted_input_folder_with_path,"txts")
 for f in old_files_list:
  os.remove(f)
 
 #generate single files for clustering
 MONOFILE_lines=[]
 with open(blasted_input_MONOfile_wp,"r") as infile:
  for line in infile:
   MONOFILE_lines.append(line.split())
   
 queries=[x[0] for x in MONOFILE_lines]
 queries=my_unique(queries)
 
 for q in queries:
  sel_lines=[l for l in MONOFILE_lines if l[0]==q]
  new_filename = blasted_input_folder_with_path + q +'.txts'
  with open(new_filename,"w") as outfile:
   for line in sel_lines:
    outfile.write('{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}\n'.format(line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8],line[9],line[10], line[11], line[12], line[13], line[14], line[15], line[16]))

 blasted_input_files_wp  = list_sorted_files(blasted_input_folder_with_path,".txts")
 tot_files=len(blasted_input_files_wp)
 if tot_files<1:
  raise ValueError('ERROR in run_primary_clustering: no blasted files!')
   
 
 print('Clustering...')
 
 for i,filename_with_path in enumerate(blasted_input_files_wp):
  query_ID=filename_with_path.split('/')[-1].split('.')[0]

  I_clustering_command=('{}  {} {}{}.rd  {}{}.cl {} {} >  {}{}.Icl.log'.format( output_exe_with_path, 
                                                                       filename_with_path ,
                                                                      output_folder,
                                                                        query_ID,
                                                                        output_folder,
                                                                        query_ID,
                                                                        distance_parameter,
                                                                        gap_parameter,
                                                                        logs_folder,
                                                                        query_ID ))
  
 

  # run command
  I_clustering_out, I_clustering_err, I_clustering_exit  =  os_shell(I_clustering_command , capture=True)
  
  if  I_clustering_exit ==1:
   print('{} is too small'.format(query_ID))
  elif I_clustering_exit !=0:
   raise ValueError('ERROR RUNNING CC run_primary_clustering: exit status "{}" stderr "{}" stdout "{}" ------ exiting'.format(I_clustering_exit, repr(I_clustering_out), repr(I_clustering_err)))
  
  if i%(tot_files/5.)==0:
   advancing_percentage=float(i)/tot_files
   print('{}%'.format(100*advancing_percentage))
 
 
 
 print('Clustering Done.')





### Generate .clsingle files, to be used for secondary clustering
def generate_clsingle_files(IN_clusters_folder):

 filelist=list_sorted_files(IN_clusters_folder,".cl")

 for i, infilename in enumerate(filelist):
  centres_line=[]
  dist_and_lines=[]
  # read input file
  with open(infilename) as infile:
   lines=infile.readlines()
  #get query id
  query_ID=lines[0].split(' ')[10]  

  for line in lines:
   dist_and_lines.append([line.split(' ')[3],line])
   if line.split(' ')[15]=="1":
    #get centres lines
    centres_line.append(line)
   
  #sort lines wrt distance from center
  dist_and_lines.sort(key=lambda x: float(x[0]))

  for c_line in centres_line:
   c=c_line.split(' ')[1]
   out_filename=str( int(query_ID)*1000000 + int(c) ) + '.clsingle'
   with open(IN_clusters_folder+out_filename,"w") as outfile:
    outfile.write(c_line)
    lines_to_write=[]
    for d,l in dist_and_lines:
      if l.split(' ')[1]==c:
       if l.split(' ')[15]!="1":
        lines_to_write.append(l)
    for k in lines_to_write:
     try:
      outfile.write(k)
     except Exception as e:
      print(e, k, IN_clusters_folder+out_filename)
      pass


### Generate distance matrix
# actually generates pre-dmat files, which contains some extra information. However firts 3 columns of pre-dmat are i,j and d_ij
def generate_dmat(install_folder, clsingle_input_folder_with_path, output_folder, logs_folder):

 output2_exe_with_path = install_folder + 'gen_dmat.x'
 
 all_clusters_filename              = 'all_original_clusters.dat'
 sorted_all_clusters_filename       = 'Sall_original_clusters.dat'
 primary_cl_pop_filename            = 'primary_cl_pop.dat'
 cpp_input_filename                 = 'cpp_input.dat'
 dmat_clean_filename                = 'pre-dmat.txt' # Pre-matrix file 
 
 all_clusters_filename_with_path           = output_folder + all_clusters_filename 
 sorted_all_clusters_filename_with_path    = output_folder + sorted_all_clusters_filename 
 primary_cl_pop_filename_with_path         = output_folder + primary_cl_pop_filename
 cpp_input_filename_with_path              = output_folder + cpp_input_filename  
 dmat_clean_wp                             = output_folder + dmat_clean_filename

 
 # 1) - GENERATE all_original_clusters.dat file
 print("- GENERATE all_original_clusters.dat file")
 filelist=list_sorted_files(clsingle_input_folder_with_path,".clsingle")
 with open(all_clusters_filename_with_path,"w") as outfile:
  for i, infilename in enumerate(filelist):
   with open(infilename) as infile:
    lines=infile.readlines()
   for l in lines:
    ll=l.split(' ')
    tw='{} {} {} {} {} {} {} {} {}\n'.format(ll[10], ll[5], ll[12], ll[13], ll[7], ll[8], ll[19], ll[20], ll[1])
    outfile.write(tw)
     
# getlines from all_original_clusters
 with open(all_clusters_filename_with_path) as infile:
  lines=infile.readlines()
 llines=[]
 for l in lines:
  llines.append(l.strip().split(' '))

 # GENERATE primary_cl_pop.dat
 print("- GENERATE primary_cl_pop.dat, a reference file with the custer ID is  (i_seq*1000000 + id_cl), and $2 gives the population of the cluster. NR will be the ID fo the cluster")
 pops={}
 idcl={}
 for line in llines:
   clid=int(line[0])*1000000+int(line[8])
   if clid in pops:
    pops[clid]=pops[clid]+1
   else:
     pops[clid]=1
 with open(primary_cl_pop_filename_with_path,"w") as outfile:
  i=0
  for x, y in pops.items():
   tw='{} {}\n'.format(x,y) 
   outfile.write(tw)
   i=i+1
   idcl[x]=i

 
 # 2) sort all_original_clusters, and keep only those sequences that generate a link across different clusters
 print("- GENERATE Sall_original_clusters.dat file (sorted wrt 2 column)")

 sllines=sorted(llines, key=lambda x: int(x[1]))

 # remove single occurrencies for s_id (2nd column) AND SORT 
 # this is awk-style... not clean but functional (ok...lazy.).
 with open(sorted_all_clusters_filename_with_path,"w") as outfile:
  o2="-1"
  c=0
  fl="" 
  for i,sl in enumerate(sllines):
   if o2 != sl[1] :
     #print("A")
    if c>0 :
      #print("B1")
      tw='{} {} {} {} {} {} {} {} {}\n'.format(fl[0], fl[1], fl[2], fl[3], fl[4], fl[5], fl[6], fl[7], fl[8])
      outfile.write(tw)
    fl=sl
    c=0
   else:
     #print("B2")
     c=c+1
     tw='{} {} {} {} {} {} {} {} {}\n'.format(sl[0], sl[1], sl[2], sl[3], sl[4], sl[5], sl[6], sl[7], sl[8])
     outfile.write(tw) 
   o2=sl[1]

 
 # Merge
 #print("merge the two previous file: and assign an ID to the clusters that correspond to NR of primary_cl_pop")
 with open(sorted_all_clusters_filename_with_path,"r") as infile:
  nlines=infile.readlines()
 nllines=[]
 for l in nlines:
  nllines.append(l.strip().split(' '))

 with open(cpp_input_filename_with_path,"w") as outfile:
  for l in nllines:
   clid=int(l[0])*1000000+int(l[8])
   tw='{} {} {} {} {} {} {} {} {} {} {}\n'.format(idcl[clid],pops[clid], l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8]) 
   outfile.write(tw)

 # 5)
 # RUN CPP PROGRAM TO GENERATE DISTANCE MATRIX
 command = '{} {} > {}'.format(output2_exe_with_path,  cpp_input_filename_with_path,dmat_clean_wp)
 print(command)
 ex1,ex2,ex3=os_shell(command, capture=True)
 error_handler_os_command(ex1,ex2,ex3,command)


 
### Run II clustering after transforming pre-dimat.txt in a true dmat input file.
def run_cpp_IIclustering(install_folder,clsingle_input_folder_with_path, output_folder, logs_folder):

 distance_parameter = 0.9 
  
 print( bcolors.OKGREEN +"Running secondary clustering using the following parameters: \n distance_parameter = {}".format(distance_parameter)+bcolors.ENDC)
 
 output_exe_with_path = install_folder + 'step2.x'
 distance_matrix_filename    = 'pre-dmat.txt'
 distance_matrix_filename_with_path = output_folder+ distance_matrix_filename
 
 
 decision_graph_filename     = 'DecisionGraph.txt'
 topography_filename         = 'Topography.info'
 pointinfo_filename          = 'Point.info'
 decision_graph_filename_wp  = output_folder + decision_graph_filename
 topography_filename_wp      = output_folder + topography_filename
 pointinfo_filename_wp       = output_folder + pointinfo_filename
 

 primary_cl_pop               = 'primary_cl_pop.dat'
 primary_cl_pop_with_path           = output_folder+primary_cl_pop



 
 
 
 #run secondary.cl
 II_clustering_command=('{} {} {} {} {} {}  '.format( output_exe_with_path,
                                                  distance_matrix_filename_with_path,
                                                  decision_graph_filename_wp,
                                                  topography_filename_wp,
                                                  pointinfo_filename_wp,
                                                  distance_parameter))     
 

 print(II_clustering_command)
 ex1,ex2,ex3=os_shell(II_clustering_command, capture=True)
 error_handler_os_command(ex1,ex2,ex3,II_clustering_command)
 print( bcolors.WARNING + 'SECONDARY CL CPP: {}'.format(ex1) + '\nSECONDARY CL CPP ERR: {}'.format(ex2) + '\n SECONDARY CL CPP EXT: {}'.format(ex3) + bcolors.ENDC )
 
 print('\n')

 
### Merge metcalusters
def merge_MCs(output_folder):

 distance_matrix_filename    = 'pre-dmat.txt'
 distance_matrix_filename_with_path = output_folder+ distance_matrix_filename

 topography_filename         = 'Topography.info'
 pointinfo_filename          = 'Point.info'
 topography_filename_wp      = output_folder + topography_filename
 pointinfo_filename_wp       = output_folder + pointinfo_filename
 
 old_topography_filename         = 'oldTopography.info'
 old_pointinfo_filename          = 'oldPoint.info'
 old_topography_filename_wp      = output_folder + old_topography_filename
 old_pointinfo_filename_wp       = output_folder + old_pointinfo_filename
 
 
 with open(distance_matrix_filename_with_path, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   ndata=int(ll[1])+1
 
 print("Found", ndata, "points (Icls). Allocating distance matrix...")

 dmat=np.ones(ndata*ndata)

 print("Reading distance matrix (Icls)")
 with open(distance_matrix_filename_with_path, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   dmat[int(ll[0]) * ndata + int(ll[1])]=float(ll[2])
   dmat[int(ll[1]) * ndata + int(ll[0])]=float(ll[2])
 #fix diagonal
 for i in range(ndata):
  dmat[i*ndata+i]=0

 print("Reading Point.info")
 assign=np.ones(ndata)
 for i,a in enumerate(assign):
  assign[i]=-10

 with open(pointinfo_filename_wp, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   clI=int(ll[0])
   clII=int(ll[5])
   assign[clI-1]=clII-1

 print("Reading Topography.info")

 with open(topography_filename_wp, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   nclII=int(ll[0])
 
 sizes=np.zeros(nclII)

 with open(topography_filename_wp, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   clII=int(ll[0])
   size=int(ll[4])
   sizes[clII-1]=size
 
 print(nclII, "MCs found")

 print("Allocate MCs dmat:")
 MCdmat=np.zeros(nclII*nclII)
 MCdcount=np.zeros(nclII*nclII)



 print("Compute MCs dmat:")
 for i in range(ndata):
  for j in range(i, ndata):
   #print(i,j,assign[i],assign[j])
   if assign[i]>=0 and assign[j]>0:
    a=int(assign[i])*nclII+int(assign[j])
    b=i*ndata+j
    MCdmat[a] += dmat[b]
    MCdcount[a] += 1
    a=int(assign[j])*nclII+int(assign[i])
    b=j*ndata+i
    MCdmat[a] += dmat[b]
    MCdcount[a] += 1

 
 print("normalize...")
 neighs=[]
 for i in range(nclII):
  for j in range(i,nclII):
   if MCdcount[i*nclII+j]>0:
    if i!=j:
     tt=MCdmat[i*nclII+j]/MCdcount[i*nclII+j]
     if tt <0.9:
      ta=[]
      ta.append(i)
      ta.append(j)
      ta.append(MCdmat[i*nclII+j])
      ta.append(MCdcount[i*nclII+j])
      ta.append(MCdmat[i*nclII+j]/MCdcount[i*nclII+j])
      neighs.append(ta)

  
 closecl=np.zeros(nclII)
 for i in range(nclII):
  closecl[i]=-10
  
 # crate tree-like list, then assign point to its new cluster by following the tree
 for i in range(len(neighs)):
  closecl[neighs[i][1]]=neighs[i][0]
  #print(neighs[i][1]) 
 newcl=np.zeros(nclII)
 for i in range(nclII):
  newcl[i]=-10
 #assignation
 for i in reversed(range(nclII)):
  if closecl[i]>=0:
   newcl[i]=int(closecl[i])
   while closecl[int(newcl[i])]>=0:
    newcl[i]=int(closecl[int(newcl[i])])
 
 
 # WRITE NEW TOPOGRAPHY AND NEW POINT INFO FILES
 # first i move the old files
 print("renaming",topography_filename_wp,old_topography_filename_wp)
 os.rename(topography_filename_wp,old_topography_filename_wp)
 print("renaming",pointinfo_filename_wp,old_pointinfo_filename_wp)
 os.rename(pointinfo_filename_wp,old_pointinfo_filename_wp)
 
 #read topography
 topolines=[]
 with open(old_topography_filename_wp, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   ta=[]
   ta.append(int(ll[0])-1)
   ta.append(int(ll[3]))
   ta.append(int(ll[4]))
   topolines.append(ta) 
 
 for i,l in enumerate(topolines):
  if newcl[l[0]]>=0 :
   topolines[i][0]=newcl[l[0]]
 
 topolines.sort(key=lambda x: x[0])
 
 #get the new clusters list
 clusters=[]
 for l in topolines:
  clusters.append(int(l[0]))
 clusters=my_unique(clusters)
 
 new_topolines=[]
 for c in clusters:
  inthiscl=[x for x in topolines if int(x[0])==c]
  thiscenter=inthiscl[0][1]
  thispop=0
  for ic in inthiscl:
   thispop+=ic[2]
  ta=[]
  ta.append(c)
  ta.append(thiscenter)
  ta.append(thispop)
  new_topolines.append(ta)
  
 #fix MCs ids (no holes, need another MC enumeration system)
 new_MCids=np.zeros(nclII)
 for i in range(nclII):
  new_MCids[i]=-10
 for i,l in enumerate(new_topolines):
  new_MCids[int(l[0])]=i

 #print new topography
 with open(topography_filename_wp, "w") as outfile:
  for l in new_topolines:
   tw="{} x x {} {} {}\n".format(int(new_MCids[int(l[0])]+1),int(l[1]),int(l[2]),int(l[2]))   
   outfile.write(tw) 
  
 #read pointinfo
 pointlines=[]
 with open(old_pointinfo_filename_wp, "r") as infile:
  for l in infile:  
   ll=l.strip().split(' ')
   ta=[]
   ta.append(int(ll[0])-1)
   ta.append(int(ll[5])-1)
   pointlines.append(ta) 

 for i,l in enumerate(pointlines):
  if l[1]>=0:   
   if newcl[l[1]]>=0 :
    pointlines[i][1]=new_MCids[int(newcl[l[1]])]
   else:
    pointlines[i][1]=new_MCids[int(l[1])]  
  
#print new pointinfo
 with open(pointinfo_filename_wp, "w") as outfile:
  for l in pointlines:
   tw="{} x x x x {} {}\n".format(int(l[0]+1),int(l[1]+1),int(l[1]+1))   
   outfile.write(tw) 
 
 






def write_MCs_fastas(output1folder,output2folder, MCfolder):
  
 primary_cl_pop_wp = output2folder +'primary_cl_pop.dat'
 pointinfo_wp = output2folder + 'Point.info'
 clsingle_metacl_wp= output2folder + 'clsingle_metacl.txt'
 
 theid=[]
 with open(primary_cl_pop_wp, "r") as infile:
  for l in infile:  
   tl=l.strip().split(' ')
   theid.append(tl[0])
 themc=[]
 with open(pointinfo_wp, "r") as infile:
  for l in infile:
   tl=l.strip().split(' ')
   themc.append(tl[6])
   
 with open(clsingle_metacl_wp,"w") as outfile:
  for i,iid in enumerate(theid):
   if(int(themc[i])>=0):
    tw='{} {}\n'.format(iid,themc[i]) 
    outfile.write(tw)

 #read the clustered clsingles
 clsingle_mc=[]
 with open(clsingle_metacl_wp,"r") as infile:
  for line in infile:
   ta=[]
   ta.append(line.split()[0])
   ta.append(line.split()[1])
   clsingle_mc.append(ta)
 
 metaclusters_list=[x[1] for x in clsingle_mc]
 metaclusters_list=my_unique(metaclusters_list)
 
 
 
 for mc in metaclusters_list:
  #get list of all the primary clusters in mc:
  Icl_list=[x[0] for x in clsingle_mc if x[1]==mc]
   
  #generate fasta with all the sequences
  fasta_toMSA_filename_wp=MCfolder+'{}.fasta'.format(mc)
  #print(fasta_toMSA_filename)
  sequences_list=[]
  for Icl in Icl_list:
   clsinglefile_toread=output1folder+'{}.clsingle'.format(Icl)
   #print("Reading primary cluster {}".format(clsinglefile_toread))
   with open(clsinglefile_toread,"r") as infile:
    for line in infile:
     ta=[]
     protein_id=line.split()[5]
     start=line.split()[7]
     end=line.split()[8]
     #seq_id='{}|{}-{}'.format(line.split()[5],line.split()[7],line.split()[8])
     sequence=  line.split()[17]
     #clean the sequence from dashes
     sequence = sequence.translate({ord("-"): None})
     ta.append(protein_id)
     ta.append(start)
     ta.append(end)
     ta.append(sequence)
     sequences_list.append(ta)
  
  sequences_list.sort(key=lambda x: (x[0], x[1],x[2]))
  
  # DELETE SEQUENCES THAT DID NOT CONTRIBUTE TO THE DISTANCE BETWEEN PRIMARY CLUSTERS
  # first get all the ids of the searchs proteins (we are working on the search space)
  search_prot_ids=[x[0] for x in sequences_list]
  search_prot_ids=my_unique(search_prot_ids)
  # check for single protein ids (it is obvious that they cannot partecipate to the distance being alonge)
  tmp1 = [ x for x in search_prot_ids if len([y[0] for y in sequences_list if y[0]==x])>1]
  search_prot_ids = tmp1
  # check for overlaps
  good_seqs=[]
  for pid in search_prot_ids:
   these_seqs=[x for x in sequences_list if x[0]==pid]
   for seqi in these_seqs:
    for seqj in these_seqs:
     if seqi!= seqj:
      if calculate_segments_overlap(int(seqi[1]),int(seqi[2]),int(seqj[1]),int(seqj[2]))>0.8: 
       good_seqs.append(seqi)
       break
 
  # in case i have several copies of the same sequence,
  # for each protein id get the avg start, the avg end, and find the closest sequence. use that.
  good_pids=my_unique([x[0] for x in good_seqs])
  seqs_to_use=[]
  for pid in good_pids:
   these_seqs=[x for x in good_seqs if x[0]==pid]
   avg_start=sum([int(x[1]) for x in these_seqs])/len(these_seqs)
   avg_end=sum([int(x[2]) for x in these_seqs])/len(these_seqs)
   best_ov=0
   best_seq='NONE' 
   for seq in these_seqs:
    ov=calculate_segments_overlap(int(seq[1]),int(seq[2]),avg_start,avg_end)
    if ov>best_ov:
     best_ov=ov
     best_seq=seq
   if best_seq != 'NONE':
    seqs_to_use.append(best_seq)
 
   
  
  with open(fasta_toMSA_filename_wp,"w") as outfile:
   if(len(seqs_to_use)>0):
    print("MC {}: writing in {}".format(mc, fasta_toMSA_filename_wp))
    for line in seqs_to_use:
     outfile.write('>{}|{}-{}\n{}\n'.format(line[0],line[1],line[2],line[3]))
   else:
    print("MC {}: **empty** - no sequences to write.".format(mc)) 





















