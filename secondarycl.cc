#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;


//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
/////////////////////////////////   M   A   I   N    //////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------


// USAGE:  ./a.out  input  output(rhosanddeltas) output(topography) output(pointinfo) dpar 
// dove input is a square matrix of the distances quadrata without the diagonal, in the form:
// i j d_ij.


int main(int argc, char** argv) {

    srand (200419901);

	double dpar; // kernel size - distance parameter
	// READ FROM OUTSIDE
  	dpar=atof(argv[5]);
	
	int ndata;
	int Icl_i, Icl_j; 
	double d_ij;
	vector<int> centres;
	vector<int> dimcl;
	vector<double> ds; // distance matrix
	vector<double> rho; // denity vector
	vector<double> delta; // delta vector
	vector<double> isc; // "is a center" vector (1: point i is a vector,  0 it isn't)
	
	ifstream infile; // input 
    ofstream dcoutfile; // output decision graph
	ofstream tpoutfile; // output topography
	ofstream pioutfile; // output point info

	string line; 
	
	// Open Input
	infile.open (argv[1]);
	if (!infile.is_open()) { cout << "INFILE NOT GOOD" << endl; return 1; }
	cout << "\n \n    Input File:        " << argv[1] << endl;
	// Get ndata:
	while( getline(infile, line)) {
		istringstream iss(line); 
		iss >> Icl_i >> Icl_j >> d_ij;  	
  		}
	  infile.close();
    ndata=Icl_j+1;
 
    // allocate distance matrix:
    ds.resize( ndata*ndata,10);
 
 
 	// ++++++++++++++++++++++++++++++++++ READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	infile.open (argv[1]);
	if (!infile.is_open()) { cout << "INFILE NOT GOOD" << endl; return 1; }

	while( getline(infile, line)) {
		istringstream iss(line); 
		float t1, t2, t3, t4, t5;
		iss >> Icl_i >> Icl_j >> d_ij >> t1 >> t2 >> t3 >> t4 >> t5; 
		// fix d_ij to 10 when extremal
		if(d_ij==1.) d_ij=10;
  		ds[Icl_i*ndata+Icl_j]=d_ij;
  		ds[Icl_j*ndata+Icl_i]=d_ij;
				}
	  infile.close();
	  
	// fix diagonal to 0  
    for(int i=0;i<ndata;++i) ds[i*ndata+i]=0;


 // open output files

 cout << "\n \n    Topography Output File:        " << argv[3] << endl;
 tpoutfile.open (argv[3]);
 if (!tpoutfile.is_open()) { cout << "OUTFILE NOT GOOD" << endl; return 1; }

 cout << "\n \n    Point Output File:        " << argv[4] << endl;
 pioutfile.open (argv[4]);
 if (!pioutfile.is_open()) { cout << "OUTFILE NOT GOOD" << endl; return 1; }

 cout << "\n \n    Decision-Graph File:        " << argv[2] << endl;
 dcoutfile.open (argv[2]);
 if (!dcoutfile.is_open()) { cout << "OUTFILE NOT GOOD" << endl; return 1; }
   
//allocate density, delta and isc vectors
rho.resize( ndata,0);
delta.resize( ndata,0);
isc.resize( ndata,0);
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE density

    // initialize with self-density
    for(int i=0;i<ndata;++i) 	rho[i] = 1. ;
    
    for(int i=0; i<ndata;++i) { 
    	for(int j=i+1; j<ndata;++j){
    		double g; // kernel
   			double d;
   			
   			d=ds[ndata*i+j];
   			
   			// stepwise kernel:
   			if(d>=dpar) g=0; else g=1.;
   			rho[i] += g  ;
    		rho[j] += g  ;
    		
    	}
    }
    
   
    
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE DELTA
        
    for(int i=0; i<ndata;++i) {
    	delta[i]=20;
    	for(int j=0; j<ndata;++j){
    		
    		if(i==j) continue;
    		if(rho[j] <= rho[i]) continue;
    		
    		double d;
    		d = ds[ndata*i+j];
    		if( d < delta[i] ) delta[i]=d; 
    		
    		    	    } 
    	
    }
   
    
    // -----------------------------------------------------------------------------------------------------------------------    
    // PRINT DECISION GRAPH    
    for(int i=0; i<ndata;++i) {
    dcoutfile << i << " " << rho[i] << " " << delta[i] << endl;
    }
   
   
    // -----------------------------------------------------------------------------------------------------------------------    
    //  FIND FIRST 200 POSSIBLE CENTERS USING GAMMA
    
   for(int npc=0; npc<(ndata)/2; ++npc){
	
		double maxgamma=0;
		int maxi=-1;
		for(int i=0; i<ndata;++i) {
		 double gamma; 
		 gamma = rho[i] * delta[i];
		 if( (gamma > maxgamma) && isc[i] !=1 ) {maxgamma = gamma; maxi=i;}
		}
	
		isc[maxi]=1;
	
		centres.push_back(maxi);
 		
    }



  	// SAVE ONLY THOSE CENTERS WITH delta=10	
   for(int c=0; c<centres.size(); ++c) if(delta[centres[c]]<9 || rho[centres[c]]<=1 )  isc[centres[c]]=0;
   // delete non-centers from centers list
   for(int c=centres.size()-1; c>=0; --c) if(isc[centres[c]]==0) centres.erase(centres.begin() + c);
      
  	//cout centers:
  	cout<< " CENTRES : " << endl;
  	for(int c=0; c<centres.size(); ++c)
  	cout << centres[c] << " " <<  rho[centres[c]]  << " " <<  delta[centres[c]] << endl;
   

  
    // -----------------------------------------------------------------------------------------------------------------------    
    //  CLUSTERING
    
    dimcl.resize(centres.size(),0);
       
    int incl=0;
    for(int i=0; i<ndata; ++i){
    	double mind=1000001;
    	int cl=-1;
    	for(int c=0; c<centres.size(); ++c){
    		double d = ds[ndata*i + centres[c]];
    		if(d < mind) { mind=d; cl=c; }
    	}
    
    	if(cl>-1){
    	double d = ds[ndata*i + centres[cl]] ;     	  
    	if(d>dpar) cl=-10;
    	pioutfile << i+1 << " x x x x " <<  cl+1 << " "  <<  cl+1 << endl;
    	++dimcl[cl];
    	++incl;
    	}
    	else {
    	cout << " \n\nERROR CUSTERIZING" << endl;
    	return 2;    	
    	}
    
    }
    
    
    for(int c=0; c<centres.size(); ++c){
    
     tpoutfile << c+1 << " x x " << centres[c]+1 << " " << dimcl[c] << " " << dimcl[c] << endl;
    
    }
    
    
    
   
   pioutfile.close();
   tpoutfile.close();
   dcoutfile.close();

   cout << "\n\n INCL=" << incl << " NDATA=" << ndata << " diff:" << ndata-incl << " ratio:" << double(incl)/ndata << endl; 

      
      
      
      
      
      
	cout << endl;   
	return 0;




}





