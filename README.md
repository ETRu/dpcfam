# INTRODUCTION
This repository contiains the implementation of DPC-based algorithm as described in Russo, E.T., Laio, A. & Punta, M. Density Peak clustering of protein sequences associated to a Pfam clan reveals clear similarities and interesting differences with respect to manual family annotation. *BMC Bioinformatics* **22**, 121 (2021). https://doi.org/10.1186/s12859-021-04013-x.
Note that the implementation has been written with the puropose of analysing, on a traditional workstation (8GB ram, 4-8 cores), query datasets with up to 5000 proteins, as those analysed in the reference paper.

# REQUIREMENTS
The main code is written in python3 (specifically using python 3.6), it is not compatible with 2.* versions

The following python3 libraries are needed:
- numpy

C++ modules are included, which requires a 
- c++ compiler (g++)

In case you want to analyse raw data and generate alignments using blast, the following third party softwares are be needed from the Blast+ suite (Ref: ... ):
- blastp
- makeblastdb 


# INSTALLATION

The installation consists in three steps:

**1. Modify the file `install_DPCF.py` as it follows:**

In this file you specify you DATA folder, where you will store input and output data.
Modify:
- `data_folder` = 'absolute path to the folder where you will store your data (see Data Folder Structure paragraph)' .
Example: `data_folder='/scratch/mydata/'`. Remember to put the final backslash.
This folder does not need to already exist.

In case you are using *raw data* (unaligned proetin sequences), you'll need to run blast to obtain pairwise alignments.
You will need a blast database too.
Modify also the following: [see Data Preparation paragraph]:
- `blast_binary` = 'path (or call) to the blastp executable'
- `blast_database_folder` = 'database (blasted formatted) location (see database paragraph)'
- `blast_num_threads`  =  1 (blastp can be run with parallel threads. We pre-set 1, but we suggest to use 4 threads in case you have few cores (up to 8); otherwise a reasonable rule is to use NCORES-5 threads)

**NOTE**: default installation paths are those used in the development computer; thus without any modifications in the install_DPCF.py file your execution will fail!
I decided to leave the development installation folders to let the user better understand how to modify them.


**2. Compile c++ modules**

- run the compile_cppmodules.sh script ( `. compile_cppmodules.sh` )

Standard compiler used is g++ with -O3 optimization. Feel free to change compiler or optimization by modyfing the commands in compile_cppmodules.sh . 

[TODO: use a CMAKE file.]

**3. Build the data folder structure running the main program with the -inst option, specifying a name for your first dataset:**

    ./DPCF.py -inst -i my_dataset

This run will build the data folder and the data folder tree for the dataset *my_dataset* (see *Data folder organization* paragraph).

After this "installation", you will need to populate the inner folders with the proper inputs (see *Data preparation* section).

I suggest you to run a "-inst" every time you want to work with a new dataset, to have the correct data folder tree.


## Data folder organization

When installed, the program will create in data_folder the my_dataset folder.
my_dataset folder contains the following folders:
- **fastas-in** will contain the sequences of the proteins of your dataset
- **blasted-outputs** will contain the alignments
- **output1** will contain intermediate results after primary clustering
- **output2** will contain intermediate results after metaclustering
- **MCLs**  will contain final MCLs
- **logs**   will contain intermediate steps log files


# QUICK START with testing dataset (using aligned data)
You can immediately run the code analyzing the testing dataset here provided (*testing_dataset* folder). 
1) Run `./DPCF.py -inst -i test`
2) Copy the all_blasted_out.txt file in data_folder/test/blasted-outputs/
3) Run: `./DPCF.py -i test -allnob`


You can run a similar analysis with any set of aligned sequences collected in a all_blasted_out.txt properly formatted  (see DATA FILES FORMATS).
Then just run `./DPCF.py -i my_dataset -allnob`.
Avoid datasets larger than 4k sequences.



# DATA PREPARATION

DPCfam allow you to analyse two types of data:


- **ALIGNED DATA**: A dataset of pairwise alignments you already obtained in some other way. In this case, you'll need to re-format your data in an "all_blasted_out.txt" (see *Data files formats* section)

- **RAW DATA**:  [*Requires blastp and a blast database*] A dataset of protein sequences to be scanned using blast against a blast database. 



# DATA PREPARATION - Aligned Data Case

In case you already have your alignments, please check that they contain all the information required for the algorithm and format it as it is decribed in the *Data Files Format* section (Alignments format).
All the alignment must be collected in a single file named *all_blasted_out.txt* and properly formatted; this file must be located in *data_folder/my_dataset/blasted-outputs/ *

# DATA PREPARATION - Raw Data Case

In case you have not already aligned data, you can use blast to perform the alignment using this program.
You will need two files:
- a fasta **dataset** of proteins to be analyised
- a fasta **database** to be scanned with blast.

**(!)** *both the dataset and the database **must use integer numbers as sequence identifiers**.*
It is not needed to use the same enumeration (if the same protein appears both in the dataset and in the database, it can use two different numbers ad sequence identifier); however, we strongly suggest to have consistency within the two enumeration systems.

In REF[] we used two *datasets* of proteins (PUA\_UR50 and P53\_UR50) and Uniref50 as *database*. However, the dataset and the database may coincide.


## Blast database preparation

The database has to be prepared using blast's make database *makeblastdb* program. In your database folder, containing the database yourdb.fasta, run:

    makeblastdb -in yourdb.fasta -parse_seqids -dbtype prot
   
  This will create a set of .psq, .pin, .phr, .psi, .psd and .pog files that will be the blast version of the database.fasta files. The *blast_database_files* install variable will be in this case *your_database_folder/yourdb*


## Fasta input file preparation

You need to collect all your fastas sequences in a single fasta file, to be named *all.fastas* and putted in the *data_folder/my_dataset/fastas-in/* directory.
Please remember that the sequences ids must be integer numbers, and contain no other information about the protein.

See the *testing_dataset/all.fasta* file in this reposiroty for a reference.



# RUNNING

After you prepared your data and stored in teh correct folder, Dpcfam runs through the following command:

	./DPCF.py -i my_dataset -*options*

To see the run options you can run `./DPCF.py -h` :

usage: DPCF.py [-h] [-inst] [-i INPUT_DATASET] [-b] [-cl1] [-cl2] [-bMC]
               [-all] [-allnob]


optional arguments:

  -h, --help            show this help message and exit

  -inst, --install      Installation: builds main data_folder folders for thedataset to analyse.

  -i INPUT_DATASET, --input_dataset INPUT_DATASET the dataset to analyse

  -b, --blast           run blast on raw fasta data. Input folder: "fasta-in", output folder: "blasted-outputs"

  -cl1, --primary_clustering run primary clustering, detects portions in the proteins that align often. Input folder: "blasted-outputs", output folder: "output1", clustering logs stored in "logs" folder
  
-cl2, --secondary_clustering run secondary clustering, metaclustering between primary clusters. Input folder: "output1", output folder: "output2"

  -bMC, --build_MCs     Build final metaclusters fastas. Input folder: "output2", "output1", "blasted-outputs; output folder "MCs"

  -all, --run_all       run the whole analysis using raw data

  -allnob, --run_all_but_blast          run the whole analysis using aligned data.





## Run with PUA_UR50/P53_UR50 dataset

At https://zenodo.org/record/3934399#.XykuL5MzbcI we provide the aligned data for the dataset used in the reference paper.

E.g., for *P53_UR50*: 
- download zip file at https://zenodo.org/record/3934399#.XykuL5MzbcI and search for teh files in the P53_UR50 folder.
- build its data folder using `./DPCF.py -inst -i P53_UR50`
- copy *all_blasted_out.txt* in *data_folder/my_dataset/blasted-outputs/*
- run  `./DPCF.py -allnob -i P53_UR50`

Runtime may require some hours.

# DATA FILES FORMATS

## Alignments format
Blast alignments must be formatted using the following blastp formatting option:

	-outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue bitscore score gapopen gaps qseq sseq"

All the alignment are collected in a single file named all_blasted_out.txt and located in data_folder/my_dataset/blasted-outputs/ 

Non-blast alignment can be given, provided that they are converted in the same format.

Each line of this file contains an alignment, with the following columns:

1. query_id (numerical, integer)
2. search_id (numerical, integer)
3. q_start (start position of the query alignment)
4. q_end (end position of the query alignment)
5. s_start (start position of the search alignment)
6. s_end (end position of the search alignment)
7. qlen (query full lenght)
8. slen (search full lenght)
9. alignment length
10. percent identity (*)
11. e-value 
12. bitscore (*)
13. score
14. number of opened gap (*)
15. number of gaps (*)
16. query aligned sequence (uppercase, with dashes as gaps)
17. search aligned sequence (uppercase, with dashes as gaps)

(columns with an asterisk contain data not necessary for the clustering program; they can be substituted with "1".)



## Primary clusters format
Using -cl1 option on a set of alignment will produce in *data_folder/my_dataset/output1* a "query_id.cl" file for each query found in the alignments. 
If the query hasn't a reasonable number of alignments, the clustering program fails and no .cl file is produced (a warning "xxx is too small will be printed when clustering).

.cl files contains one "clustered alignment" for each line, with the following columns:

1. Alignment id (integer)
2. Cluster Center id (integer)
3. Cluster id (integer) cl
4. Distance of the alignment from the cluster center
5. "|"
6. search_id
7. "notxt"
8. s_start
9. s_end
10. "|"
11. query_id
12. "notxt"
13. q_start
14. q_end
15. "|"
16. isc (1 if this alignment is a center, 0 if it isnt)
17. "|"
18. search aligned sequence (uppercase, with dashes as gaps)
19. query aligned sequence (uppercase, with dashes as gaps)
20. score/(q_end-q_start)
21. e-value


## Secondary clusters: Topography.info and Point.info

As a first output of the secondary clustering, in *data_folder/my_dataset/output2*  you will fin two files: Topography.info and Point.info. These files are fomatted in teh same fashon ad tehr DPA outputs [reference: ]

**Point.info** stores, for each point (primary cluster) the metacluster id to which it is assigned. Each line stores a primary cluster with an incrementing id, as it follows:

`cl_id x x x x MC_id MC_id`

If MC_id is -9, then the point has no MC assignation.
(*x* character and duplicate columns are needed to fill unused columns of the standard DPA output files)


**Topography.info** stores, for each metaluster, the incremental metacluster, the respective cluster center and its population (in terms of primary cluster). 

`MC_id x x center population`

(*x* character are needed to fill unused columns of teh standard DPA output files)


## Final output

Final output is stored in *data_folder/my_dataset/MCs* and consists of a fasta file for each MC collecting all teh clustered protein sequences, with no gaps (not full proteins but aligned chunks). The id of each sequence containd the s_id of the original protein and the start and end position of the alingment:

`>s_id|sstart-ssend` 



